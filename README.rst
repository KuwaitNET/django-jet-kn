====================
Kuwaitnet Django JET
====================

.. image:: https://travis-ci.org/geex-arts/django-jet.svg?branch=master
    :target: https://travis-ci.org/geex-arts/django-jet

**Modern template for Django admin interface with improved functionality based on Django-Jet and add more features**


Why Kuwaitnet Django JET?
=========================

* New fresh look
* Responsive mobile interface
* Useful admin home page
* Minimal template overriding
* Easy integration
* Themes support
* Autocompletion
* Handy controls


Installation
============

* Download and install latest version of Django JET:

.. code:: python

    pip install -e git+https://bitbucket.org/KuwaitNET/django-jet-kn.git@master#egg=jet

* Add the following to the INSTALLED_APPS setting (or DJANGO_APPS if you use that style) of your Django project settings.py file (Please note 'jet' and 'jet.jet_dashboard' should be before 'django.contrib.admin'):

.. code:: python

    INSTALLED_APPS = (
        ...
        'django.contrib.sites',
        <User App>
        ...
        'modeltranslation',
        'import_export',
        'django_extensions',
        'solo',
        'jet.jet_dashboard',
        'jet',
        "django.contrib.admin",
        'adminsortable',

    )
        
* Make sure ``django.template.context_processors.request`` context processor is enabled in settings.py (Django 1.8+ way):

.. code:: python

    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    ...
                    'django.template.context_processors.request',
                    ...
                ],
            },
        },
    ]

.. warning::
    Before Django 1.8 you should specify context processors different way. Also use ``django.core.context_processors.request`` instead of ``django.template.context_processors.request``.

    .. code:: python

        from django.conf import global_settings

        TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
            'django.core.context_processors.request',
        )

* Add URL-pattern to the urlpatterns of your Django project urls.py file (they are needed for related lookups and autocompletes):

.. code:: python

    urlpatterns = patterns(
        ...
        url(r'^jet/dashboard/', include('jet.jet_dashboard.urls', 'jet-dashboard')),
        url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
        url(r'^admin/', include(admin.site.urls)),
        ...
    )

* Create database tables:

.. code:: python

    python manage.py migrate jet
    # or 
    python manage.py syncdb
        
* Collect static if you are in production environment:

.. code:: python

        python manage.py collectstatic
        
* Clear your browser cache

* Add URL-pattern to the urlpatterns of your Django project urls.py file (they are needed for related lookups and autocompletes):

.. code:: python

    urlpatterns = patterns(
        '',
        path(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
        path(r'^jet/dashboard/', include('jet.jet_dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
        path('admin/', admin.site.urls),
        ...
    )

* **For Google Analytics widgets only** install python package:

.. code::

    pip install google-api-python-client==1.4.1

* Create database tables:

.. code:: python

    python manage.py migrate dashboard
    # or
    python manage.py syncdb

* Collect static if you are in production environment:

.. code:: python

        python manage.py collectstatic



