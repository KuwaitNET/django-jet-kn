from django.contrib import admin
from django.contrib.auth.admin import UserAdmin, Group, GroupAdmin
from django.contrib.auth import get_user_model
from django.core.exceptions import ImproperlyConfigured
from django import forms
from django.utils import translation
from django.apps import apps
from django.utils.encoding import force_str
from .widgets import TabularPermissionsWidget
from . import settings
from .models import PermissionAppHelp, PermissionModelHelp

User = get_user_model()


class CompactInline(admin.options.InlineModelAdmin):
    template = 'admin/edit_inline/compact.html'


class UserTabularPermissionsMixin(object):
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        field = super(UserTabularPermissionsMixin, self).formfield_for_manytomany(db_field, request, **kwargs)
        if db_field.name == 'user_permissions':
            field.widget = TabularPermissionsWidget(db_field.verbose_name, db_field.name in self.filter_vertical)
            field.help_text = ''
        return field


class GroupTabularPermissionsMixin(object):
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        field = super(GroupTabularPermissionsMixin, self).formfield_for_manytomany(db_field, request, **kwargs)
        if db_field.name == 'permissions':
            field.widget = TabularPermissionsWidget(db_field.verbose_name, db_field.name in self.filter_vertical,
                                                    'permissions')
            field.help_text = ''
        return field

class TabularPermissionsUserAdmin(UserTabularPermissionsMixin, admin.site._registry[User].__class__):
    pass


class TabularPermissionsGroupAdmin(GroupTabularPermissionsMixin, GroupAdmin):
    pass


if settings.AUTO_IMPLEMENT:
    try:
        admin.site.unregister(User)
        admin.site.register(User, TabularPermissionsUserAdmin)
        admin.site.unregister(Group)
        admin.site.register(Group, TabularPermissionsGroupAdmin)

    except:
        raise ImproperlyConfigured('Please make sure that django.contrib.auth '
                                   'comes before tabular_permissions in INSTALLED_APPS')


def get_model():
    # translation.activate('en')
    MODEL = []
    for app in apps.get_app_configs():
        try:
            for model in app.get_models():
                model_name = force_str(app.verbose_name) + " / " + force_str(model._meta.verbose_name)
                MODEL.append((model_name, model_name))
        except:
            pass
    return MODEL


class PermissionModelHelpAdminForm(forms.ModelForm):
    app_model = forms.ChoiceField(choices=get_model())

    class Meta:
        exclude = []
        model = PermissionModelHelp


class PermissionModelHelpAdmin(admin.ModelAdmin):
    form = PermissionModelHelpAdminForm


class PermissionAppHelpAdminForm(forms.ModelForm):
    APP = [(app.verbose_name, app.verbose_name) for app in apps.get_app_configs() if
           app.label not in ['sessions', 'contenttypes', 'admin']]
    app = forms.ChoiceField(choices=APP)

    class Meta:
        exclude = []
        model = PermissionAppHelp


class PermissionAppHelpAdmin(admin.ModelAdmin):
    form = PermissionAppHelpAdminForm


admin.site.register(PermissionModelHelp, PermissionModelHelpAdmin)
admin.site.register(PermissionAppHelp, PermissionAppHelpAdmin)

from .jet_dashboard.admin import *