from django import template
from django.utils.translation import ugettext_lazy as _

from jet.models import PermissionAppHelp, PermissionModelHelp

register = template.Library()


@register.simple_tag
def get_app_help_string_permission(app_verbose):
    return PermissionAppHelp.objects.filter(app=app_verbose).first().help_text if \
        PermissionAppHelp.objects.filter(app=app_verbose).exists() else ""


@register.simple_tag
def get_show_app_in_permission(app_verbose):
    return PermissionAppHelp.objects.filter(app=app_verbose).first().show_in_permission if \
        PermissionAppHelp.objects.filter(app=app_verbose).exists() else True


@register.simple_tag
def get_model_help_string_permission(app_verbose, model_verbose):
    app_model_verbose = app_verbose + " / " + model_verbose
    return PermissionModelHelp.objects.filter(app_model=app_model_verbose).first().help_text if \
        PermissionModelHelp.objects.filter(app_model=app_model_verbose).exists() else ""


@register.simple_tag
def get_show_model_in_permission(app_verbose, model_verbose):
    app_model_verbose = app_verbose + " / " + model_verbose
    return PermissionModelHelp.objects.filter(app_model=app_model_verbose).first().show_in_permission if \
        PermissionModelHelp.objects.filter(app_model=app_model_verbose).exists() else True