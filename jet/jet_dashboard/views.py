import json
from django.contrib import messages
from django.core.exceptions import ValidationError
try:
    from django.core.urlresolvers import reverse
except ImportError: # Django 1.11
    from django.urls import reverse

from django.shortcuts import redirect, HttpResponse
from django.db.models import Q
from django.forms.formsets import formset_factory
from django.http import HttpResponseRedirect
from django.views.decorators.http import require_POST, require_GET
from jet.jet_dashboard.forms import UpdateDashboardModulesForm, AddUserDashboardModuleForm, \
    UpdateDashboardModuleCollapseForm, RemoveDashboardModuleForm, ResetDashboardForm
from jet.jet_dashboard.models import UserDashboardModule
from jet.utils import JsonResponse, get_app_list, SuccessMessageMixin, user_is_authenticated
from django.views.generic import UpdateView
from django.utils.translation import activate, ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.apps import apps
from django.contrib import admin
from django.utils.encoding import force_str

from jet.jet_dashboard.models import UserDashboardModule


User = get_user_model()


class UpdateDashboardModuleView(SuccessMessageMixin, UpdateView):
    model = UserDashboardModule
    fields = ('title', 'title_ar')  # Remove this if no need for Arabic (remove 'title_ar')
    template_name = 'jet.dashboard/update_module.html'
    success_message = _('Widget was successfully updated')
    object = None
    module = None

    def has_permission(self, request):
        return request.user.is_active and request.user.is_staff

    def get_success_url(self):
        if self.object.app_label:
            return reverse('admin:app_list', kwargs={'app_label': self.object.app_label})
        else:
            return reverse('admin:index')

    def get_module(self):
        object = self.object if getattr(self, 'object', None) is not None else self.get_object()
        return object.load_module()

    def get_settings_form_kwargs(self):
        kwargs = {
            'initial': self.module.settings
        }

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def get_settings_form(self):
        if self.module.settings_form:
            form = self.module.settings_form(**self.get_settings_form_kwargs())
            if hasattr(form, 'set_module'):
                form.set_module(self.module)
            return form

    def get_children_formset_kwargs(self):
        kwargs = {
            'initial': self.module.children,
            'prefix': 'children',
        }

        if self.request.method in ('POST', 'PUT'):
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def get_children_formset(self):
        if self.module.child_form:
            return formset_factory(self.module.child_form, can_delete=True, extra=1)(**self.get_children_formset_kwargs())

    def clean_children_data(self, children):
        children = list(filter(
            lambda item: isinstance(item, dict) and item and item.get('DELETE') is not True,
            children
        ))
        for item in children:
            item.pop('DELETE')
        return children

    def get_current_app(self):
        app_list = get_app_list({'request': self.request})

        for app in app_list:
            if app.get('app_label', app.get('name')) == self.object.app_label:
                return app

    def get_context_data(self, **kwargs):
        data = super(UpdateDashboardModuleView, self).get_context_data(**kwargs)
        data['title'] = _('Change')
        data['module'] = self.module
        data['settings_form'] = self.get_settings_form()
        data['children_formset'] = self.get_children_formset()
        data['child_name'] = self.module.child_name if self.module.child_name else _('Items')
        data['child_name_plural'] = self.module.child_name_plural if self.module.child_name_plural else _('Items')
        data['app'] = self.get_current_app()
        return data

    def dispatch(self, request, *args, **kwargs):
        if not self.has_permission(request):
            index_path = reverse('admin:index')
            return HttpResponseRedirect(index_path)

        self.object = self.get_object()
        self.module = self.get_module()(model=self.object)
        return super(UpdateDashboardModuleView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        settings_form = self.get_settings_form()
        children_formset = self.get_children_formset()

        data = request.POST.copy()

        if settings_form:
            if settings_form.is_valid():
                settings = settings_form.cleaned_data
                data['settings'] = self.module.dump_settings(settings)
            else:
                return self.form_invalid(self.get_form(self.get_form_class()))

        if children_formset:
            if children_formset.is_valid():
                self.module.children = self.clean_children_data(children_formset.cleaned_data)
                self.module.children.sort(key=lambda x: x['order'])
                data['children'] = self.module.dump_children()
            else:
                return self.form_invalid(self.get_form(self.get_form_class()))

        request.POST = data

        return super(UpdateDashboardModuleView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        if 'settings' in form.data:
            form.instance.settings = form.data['settings']
        if 'children' in form.data:
            form.instance.children = form.data['children']
        return super(UpdateDashboardModuleView, self).form_valid(form)


@require_POST
def update_dashboard_modules_view(request):
    result = {'error': False}
    form = UpdateDashboardModulesForm(request, request.POST)

    if form.is_valid():
        form.save()
    else:
        result['error'] = True

    return JsonResponse(result)


@require_POST
def add_user_dashboard_module_view(request):
    result = {'error': False}
    form = AddUserDashboardModuleForm(request, request.POST)

    if form.is_valid():
        module = form.save()
        result['id'] = module.pk
        messages.success(request, _('Widget has been successfully added'))

        if module.app_label:
            result['success_url'] = reverse('admin:app_list', kwargs={'app_label': module.app_label})
        else:
            result['success_url'] = reverse('admin:index')
    else:
        result['error'] = True

    return JsonResponse(result)


@require_POST
def update_dashboard_module_collapse_view(request):
    result = {'error': False}

    try:
        instance = UserDashboardModule.objects.get(pk=request.POST.get('id'))
        form = UpdateDashboardModuleCollapseForm(request, request.POST, instance=instance)

        if form.is_valid():
            module = form.save()
            result['collapsed'] = module.collapsed
        else:
            result['error'] = True
    except UserDashboardModule.DoesNotExist:
        result['error'] = True

    return JsonResponse(result)


@require_POST
def remove_dashboard_module_view(request):
    result = {'error': False}

    try:
        instance = UserDashboardModule.objects.get(pk=request.POST.get('id'))
        form = RemoveDashboardModuleForm(request, request.POST, instance=instance)

        if form.is_valid():
            form.save()
        else:
            result['error'] = True
    except UserDashboardModule.DoesNotExist:
        result['error'] = True

    return JsonResponse(result)


@require_GET
def load_dashboard_module_view(request, pk):
    result = {'error': False}

    try:
        if not user_is_authenticated(request.user) or not request.user.is_staff:
            raise ValidationError('error')

        instance = UserDashboardModule.objects.get(pk=pk, user=request.user.pk)
        module_cls = instance.load_module()
        module = module_cls(model=instance, context={'request': request})
        result['html'] = module.render()
    except (ValidationError, UserDashboardModule.DoesNotExist):
        result['error'] = True

    return JsonResponse(result)


@require_POST
def reset_dashboard_view(request):
    result = {'error': False}
    form = ResetDashboardForm(request, request.POST)

    if form.is_valid():
        form.save()
    else:
        result['error'] = True

    return JsonResponse(result)


def update_dashboard(request):
    tbl = UserDashboardModule.objects.filter(~Q(user=request.user.id))
    if tbl:
        tbl.delete()
    #     messages.success(self.request, "{0}".format("All data removed for all users except selected one"))
    # else:
    #     messages.success(self.request, "{0}".format("No data to removed"))

    ref_dashboard = UserDashboardModule.objects.filter(user=request.user.id)
    for user in User.objects.filter(~Q(id=request.user.id)).filter(Q(is_staff=True) | Q(is_superuser=True)):
        for widget in ref_dashboard:
            UserDashboardModule(title=widget.title, module=widget.module, app_label=widget.app_label,
                                user=user.id,
                                column=widget.column, order=widget.order, settings=widget.settings,
                                children=widget.children, collapsed=widget.collapsed).save()

        # messages.success(self.request, "{0}{1}".format("Updated Dashboard for the user: ", user.username))
    messages.success(request, "{0}".format("Update Other users Dashboard"))


def propagate_jet_dashboard(request):
    # site_config = SiteConfiguration.get_solo()
    if request.user.is_superuser:
        update_dashboard(request)

    return redirect('admin:index')


def clear_cache(request):
    next = request.GET.get('next', None)

    if next:
        return_url = next
    else:
        return_url = reverse('admin:index')
    try:
        call_command('clear_cache')
        messages.add_message(request, messages.INFO, _("Cache has been cleared!"))
    except:
        messages.add_message(request, messages.ERROR, _("Failed to reset cache!!!"))
    return redirect(return_url)


def get_model():
    # translation.activate('en')
    MODEL = []
    for app in apps.get_app_configs():
        try:
            for model in app.get_models():
                model_name = force_str(app.verbose_name) + " / " + force_str(model._meta.verbose_name)
                MODEL.append((model_name, model_name))
        except:
            pass
    return MODEL


def generate_custom_dashboard(request):
    next = request.GET.get('next', None)

    if next:
        return_url = next
    else:
        return_url = reverse('admin:index')
    app_dic = {}
    activate('en')
    for model in admin.site._registry:
        if model._meta.app_label not in app_dic:
            activate('en')
            app_label_en = model._meta.app_config.verbose_name
            activate('ar')
            app_label_ar = model._meta.app_label
            app_dic[model._meta.app_label] = {'app_label_en': app_label_en, 'app_label_ar': app_label_ar}

        if 'models' not in app_dic[model._meta.app_label]:
            app_dic[model._meta.app_label]['models'] = {}

        if model._meta.verbose_name not in app_dic[model._meta.app_label]:
            activate('en')
            model_label_en = model._meta.verbose_name
            activate('ar')
            model_label_ar = model._meta.verbose_name
            activate('en')
            app_dic[model._meta.app_label]['models'][model._meta.object_name] = {
                'model_label_en': model_label_en, 'model_label_ar': model_label_ar
            }
    UserDashboardModule.objects.filter(user=request.user.id).delete()
    i = 0
    objects = []
    for app_label, app in app_dic.items():

        settings = '{"header_style": "whited", "show_in_menu": true, "users": [], "groups": []}'
        title_en = app['app_label_en']
        title_ar = app['app_label_ar']
        children = []
        j = 0
        for key, model_data in app['models'].items():
            children.append(
                """{"title": "%s", "title_ar": "%s","model": "%s.%s", "url": "", "is_header": false, "title_message": "","title_message_ar": "", "order": %d, "show_in_menu": true}""" %
                (model_data['model_label_en'], model_data['model_label_ar'], app_label, key, j)
            )
            j += 1

        objects.append(
            UserDashboardModule(title=title_en, title_ar=title_ar, module="jet.jet_dashboard.modules.CustomAppList",
                                user=request.user.id, column=0, order=i, settings=settings, children='[{}]'.format(",".join(children)), collapsed=0)
        )

        i += 1

    UserDashboardModule.objects.bulk_create(objects)
    messages.add_message(request, messages.INFO, _("Dashboard is generated"))
    return redirect(return_url)


def download_my_dashboard(request):
    if request.method == 'GET':
        module_list = []
        for module in UserDashboardModule.objects.filter(user=request.user.id):
            module_list.append(
                {
                    'title': module.title,
                    'title_ar': module.title_ar,
                    'module': module.module,
                    'app_label': module.app_label,
                    'column': module.column,
                    'order': module.order,
                    'settings': module.settings,
                    'children': module.children,
                    'collapsed': module.collapsed,
                }
            )
        the_json = {'value': module_list}
        content = json.dumps(the_json)
        response = HttpResponse(content_type='application/json', content=content)
        response['Content-Disposition'] = 'attachment; filename="dashboard.json"'
        return response

    elif request.method == 'POST':
        files = request.FILES
        dashboard_config = files.getlist('dashboard_config')
        if dashboard_config:
            try:
                file_str = dashboard_config[0].read().decode()
                file_json = json.loads(file_str)
                # Loop over the module to make sure the next loop will work fine before we delete the existing
                # configuration
                for module in file_json['value']:
                    title = module['title']
                    title_ar = module['title_ar']
                    mod = module['module']
                    app_label = module['app_label']
                    user = request.user.id
                    column = module['column']
                    order = module['order']
                    settings = module['settings']
                    children = module['children']
                    collapsed = module['collapsed']

                UserDashboardModule.objects.filter(user=request.user.id).delete()
                for module in file_json['value']:
                    UserDashboardModule(title=module['title'], title_ar=module['title_ar'], module=module['module'],
                                        app_label=module['app_label'], user=request.user.id, column=module['column'],
                                        order=module['order'], settings=module['settings'], children=module['children'],
                                        collapsed=module['collapsed']).save()
            except:
                messages.add_message(request, messages.ERROR, _("Please upload a valid configuration file"))

        return redirect(reverse('admin:index'))
