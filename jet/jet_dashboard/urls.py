import django
from django.conf.urls import url

try:
    from django.views.i18n import JavaScriptCatalog

    javascript_catalog = JavaScriptCatalog.as_view()
except ImportError:  # Django < 2.0
    from django.views.i18n import javascript_catalog

from jet.jet_dashboard import dashboard
from jet.jet_dashboard.views import update_dashboard_modules_view, add_user_dashboard_module_view, \
    update_dashboard_module_collapse_view, remove_dashboard_module_view, UpdateDashboardModuleView, \
    load_dashboard_module_view, reset_dashboard_view, propagate_jet_dashboard, clear_cache, generate_custom_dashboard, \
    download_my_dashboard
from jet.jet_dashboard.dashboard_modules.google_analytics_views import google_analytics_callback_view, \
    google_analytics_grant_view, google_analytics_revoke_view

app_name = 'jet_dashboard'

urlpatterns = [
    url(
        r'^module/(?P<pk>\d+)/$',
        UpdateDashboardModuleView.as_view(),
        name='update_module'
    ),
    url(
        r'^update_dashboard_modules/$',
        update_dashboard_modules_view,
        name='update_dashboard_modules'
    ),
    url(
        r'^add_user_dashboard_module/$',
        add_user_dashboard_module_view,
        name='add_user_dashboard_module'
    ),
    url(
        r'^update_dashboard_module_collapse/$',
        update_dashboard_module_collapse_view,
        name='update_dashboard_module_collapse'
    ),
    url(
        r'^remove_dashboard_module/$',
        remove_dashboard_module_view,
        name='remove_dashboard_module'
    ),
    url(
        r'^load_dashboard_module/(?P<pk>\d+)/$',
        load_dashboard_module_view,
        name='load_dashboard_module'
    ),
    url(
        r'^reset_dashboard/$',
        reset_dashboard_view,
        name='reset_dashboard'
    ),
    url(
        r'^jsi18n/$',
        javascript_catalog,
        {'packages': 'jet'},
        name='jsi18n'
    ),
    url(r'^propagate_jet_dashboard/',
        propagate_jet_dashboard,
        name='propagate_jet_dashboard'),
    url(r'^clear_cache/',
        clear_cache,
        name='clear_cache'),
    url(r'^generate_custom_dashboard/',
        generate_custom_dashboard,
        name='generate_custom_dashboard'),
    url(r'^download_my_dashboard/',
        download_my_dashboard,
        name='download_my_dashboard'),
    url(r'^google-analytics/grant/(?P<pk>\d+)/$', google_analytics_grant_view, name='google-analytics-grant'),
    url(r'^google-analytics/revoke/(?P<pk>\d+)/$', google_analytics_revoke_view, name='google-analytics-revoke'),
    url(r'^google-analytics/callback/', google_analytics_callback_view, name='google-analytics-callback'),
]

urlpatterns += dashboard.urls.get_urls()

if django.VERSION[:2] < (1, 8):
    from django.conf.urls import patterns

    urlpatterns = patterns('', *urlpatterns)
