from importlib import import_module
from jet.jet_dashboard import settings
from .models import UserDashboardModule


def get_current_dashboard(location):
    if location == 'index':
        path = settings.JET_INDEX_DASHBOARD
    elif location == 'app_index':
        path = settings.JET_APP_INDEX_DASHBOARD
    else:
        raise ValueError('Unknown jet_dashboard location: %s' % location)

    module, cls = path.rsplit('.', 1)

    try:
        module = import_module(module)
        index_dashboard_cls = getattr(module, cls)
        return index_dashboard_cls
    except ImportError:
        return None


def get_menu_items_from_dashboard(context):
    modules = UserDashboardModule.objects.filter(user=context['request'].user.pk)
    app_list = []
    for module in modules:
        module_cls = module.load_module()
        module = module_cls(model=module, context={'request': context['request']})
        module.init_with_context(context)
        app_list.append(module)

    return app_list
