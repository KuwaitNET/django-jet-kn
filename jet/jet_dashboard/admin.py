from django.contrib import admin
from django import forms
from django.utils import translation
from django.apps import apps
from django.contrib.auth import get_user_model

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db.models import Q

from solo.admin import SingletonModelAdmin
from jet.models import JetMenuConfiguration, JetSetting
from modeltranslation.admin import TabbedDjangoJqueryTranslationAdmin
from adminsortable.admin import SortableAdmin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

from .models import UserDashboardModule


User = get_user_model()


def get_model_form_menu():
    translation.activate('en')
    MODEL = []
    for app in apps.get_app_configs():
        try:
            for model in app.get_models():
                model_name = "admin:" + str(app.name).split('.')[-1] + "_" + str(model._meta.model_name) + "_changelist"
                model_name_verbose = str(app.verbose_name) + " / " + str(model._meta.verbose_name)
                MODEL.append((model_name, model_name_verbose))
        except:
            pass
    return MODEL


class JetMenuConfigurationResource(resources.ModelResource):

    class Meta:
        model = JetMenuConfiguration


class JetMenuConfigurationAdminForm(forms.ModelForm):
    app_model = forms.ChoiceField(choices=get_model_form_menu())

    class Meta:
        exclude = []
        model = JetMenuConfiguration


@admin.register(JetMenuConfiguration)
class JetMenuConfigurationAdmin(ImportExportModelAdmin, SortableAdmin, TabbedDjangoJqueryTranslationAdmin):
    form = JetMenuConfigurationAdminForm
    resource_class = JetMenuConfigurationResource
    change_list_template_extends = 'admin/sort_import_export/change_list_sort_import_export.html'

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'users':
            kwargs['queryset'] = User.objects.filter(Q(is_staff=True) | Q(is_superuser=True))
        return super(JetMenuConfigurationAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


@admin.register(JetSetting)
class JetSettingAdmin(SingletonModelAdmin):
    pass


@receiver(post_save, sender=UserDashboardModule)
def update_jet_dashboard(sender, instance, **kwargs):
    user = User.objects.get(id=int(instance.user))
    site_config = JetSetting.get_solo()
    if site_config.jet_admin_user == user and site_config.auto_deploy_dashboard:
        tbl = UserDashboardModule.objects.filter(~Q(user=user.id))
        if tbl:
            tbl.delete()

        ref_dashboard = UserDashboardModule.objects.filter(user=user.id)
        for user in User.objects.filter(~Q(id=user.id)).filter(Q(is_staff=True) | Q(is_superuser=True)):
            for widget in ref_dashboard:
                UserDashboardModule(title=widget.title, module=widget.module, app_label=widget.app_label,
                                    user=user.id,
                                    column=widget.column, order=widget.order, settings=widget.settings,
                                    children=widget.children, collapsed=widget.collapsed).save()
