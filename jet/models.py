from django.db import models
from django.utils import timezone
try:
    from django.utils.encoding import python_2_unicode_compatible
except ImportError:
    from django.utils.six import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _, activate
from django.apps import apps
from django.shortcuts import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from adminsortable.models import Sortable
from solo.models import SingletonModel


User = get_user_model()


@python_2_unicode_compatible
class Bookmark(models.Model):
    url = models.URLField(verbose_name=_('URL'))
    title = models.CharField(verbose_name=_('title'), max_length=255)
    user = models.PositiveIntegerField(verbose_name=_('user'))
    date_add = models.DateTimeField(verbose_name=_('date created'), default=timezone.now)

    class Meta:
        verbose_name = _('bookmark')
        verbose_name_plural = _('bookmarks')
        ordering = ('date_add',)

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class PinnedApplication(models.Model):
    app_label = models.CharField(verbose_name=_('application name'), max_length=255)
    user = models.PositiveIntegerField(verbose_name=_('user'))
    date_add = models.DateTimeField(verbose_name=_('date created'), default=timezone.now)

    class Meta:
        verbose_name = _('pinned application')
        verbose_name_plural = _('pinned applications')
        ordering = ('date_add',)

    def __str__(self):
        return self.app_label


@python_2_unicode_compatible
class PermissionAppHelp(models.Model):
    # activate('en')
    # APP = [(app.verbose_name, app.verbose_name) for app in apps.get_app_configs() if app.label not in ['sessions', 'contenttypes', 'admin']]

    app = models.CharField(max_length=100, unique=True)
    help_text = models.TextField(verbose_name=_("Help Text"), null=True, blank=True)
    show_in_permission = models.BooleanField(default=True, verbose_name=_("Show in Permission Page"))

    def __str__(self):
        return self.app


# def get_model():
#     activate('en')
#     MODEL = []
#     for app in apps.get_app_configs():
#         try:
#             for model in app.get_models():
#                 model_name = str(app.verbose_name) + " / " + str(model._meta.verbose_name)
#                 MODEL.append((model_name, model_name))
#         except:
#             pass
#     return MODEL


@python_2_unicode_compatible
class PermissionModelHelp(models.Model):
    app_model = models.CharField(max_length=150, unique=True)
    help_text = models.TextField(verbose_name=_("Help Text"), null=True, blank=True)
    show_in_permission = models.BooleanField(default=True, verbose_name=_("Show in Permission Page"))

    def __str__(self):
        return self.app_model


@python_2_unicode_compatible
class JetSetting(SingletonModel):
    jet_admin_user = models.ForeignKey(User, verbose_name=_("Jet Dashboard Admin User"), null=True, blank=True,
                                       on_delete=models.SET_NULL)
    auto_deploy_dashboard = models.BooleanField(_("Auto deploy Dashboard to Users"), default=False)
    MENU_OPTIONS = (
        ("default", _("Default")),
        ("jet_dashboard", _("Jet Dashboard")),
        ("none", _("None"))
    )
    jet_dashboard_menu = models.CharField(_("Menu Source"), default='default', choices=MENU_OPTIONS, max_length=50)
    show_custom_menu = models.BooleanField(_("Show Custom Menu"), default=False)
    compact_menu = models.BooleanField(_("Use Compact Menu"), default=False)

    class Meta:
        verbose_name = _('Setting')
        verbose_name_plural = _('Settings')

    def __str__(self):
        return "Jet Setting"


@python_2_unicode_compatible
class JetMenuConfiguration(Sortable):
    ICONS = (
        ('icon-open-external', 'Open External'),
        ('icon-data', 'Data'),
        ('icon-settings', 'Settings'),
    )
    title = models.CharField(_("Title"), max_length=100)
    icon = models.CharField(max_length=100, choices=ICONS, blank=True, null=True)
    url = models.URLField(_("URL"), blank=True, null=True)
    app_model = models.CharField(max_length=150, null=True, blank=True)
    is_header = models.BooleanField(default=False)
    is_change_lang = models.BooleanField(default=False)
    users = models.ManyToManyField(User, verbose_name=_("Users"), blank=True)
    groups = models.ManyToManyField(Group, verbose_name=_("Groups"), blank=True)

    def __str__(self):
        output = []
        if hasattr(self, 'title_en') and self.title_en:
            output.append(self.title_en)
        if hasattr(self, 'title_ar') and self.title_ar:
            output.append(self.title_ar)
        return ' / '.join(output)

    def get_url(self):
        if self.url:
            return self.url
        if self.app_model:
            try:
                url = reverse(self.app_model)
                return url
            except:
                return None
        return None

    class Meta:
        ordering = ['order']
        verbose_name = _('Menu')
        verbose_name_plural = _('Menus')
