from __future__ import unicode_literals

from modeltranslation.translator import translator, TranslationOptions

from .models import JetMenuConfiguration


class JetMenuConfigurationTranslation(TranslationOptions):
    fields = ('title', )


translator.register(JetMenuConfiguration, JetMenuConfigurationTranslation)
